// app-worker.js
import express from 'express'
import hpp from 'hpp'
import helmet from 'helmet'
import { config as dotenvConfig } from 'dotenv'
import { RateLimiterCluster } from 'rate-limiter-flexible'
import { rpcNode } from './config.js'
import bodyParser from 'body-parser'

const app = express()
app.set('trust proxy', true)
app.use(helmet())
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

// more info: www.npmjs.com/package/hpp
app.use(hpp())

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  // res.header('Access-Control-Allow-Credentials', true)
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )
  next()
})

const rateLimiter = new RateLimiterCluster({
  keyPrefix: 'pm2clusterlimiter', // Must be unique for each limiter
  points: 2,
  duration: 1,
  timeoutMs: 3000 // Promise is rejected, if master doesn't answer for 3 secs
})

app.post('/', (req, res) => {
  rateLimiter
    .consume(req.ip, 1) // consume 1 point
    .then((rateLimiterRes) => {
      console.log(req.body)
      fetch(rpcNode, {
        body: JSON.stringify(req.body),
        method: 'POST'
      }).then(result => result.json()).then(result => res.json(result))
      .catch(err => res.json({
        jsonrpc: '2.0',
        error: { code: 400, message: 'Proxy error.' },
        id: 1
      }))
    })
    .catch((rateLimiterRes) => {
      // Not enough points to consume
      res.status(429).json({
        jsonrpc: '2.0',
        error: { code: 429, message: 'Too many requests.', tryAfter: rateLimiter.duration },
        id: 1
      })
    })
})

dotenvConfig()
const port = process.env.PORT || 5858
const host = process.env.HOST || '127.0.0.1'
app.listen(port, host, () => {
  console.log(`Application started on ${host}:${port}`)
})
