module.exports = {
  apps: [
    {
      name: 'master-limiter',
      script: './master-limiter.js',
      instances: 1,
      autorestart: true,
      max_memory_restart: '2G'
    },
    {
      name: 'worker-limiter',
      script: './worker-limiter.js',
      instances: 6,
      exec_mode: 'cluster',
      autorestart: true,
      max_memory_restart: '2G'
    }
  ]
}
